# .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

export PS1="\[$(tput bold)\][\u@\h \W]\\$ \[$(tput sgr0)\]"
#source ~/.cache/wal/colors-tty.sh

alias xi="sudo xbps-install"
alias xr="sudo xbps-remove"
alias xrr="sudo xbps-remove -R"
alias xq="sudo xbps-query"
